<?php
define('ROOT_NS', 'LibreByte');
spl_autoload_register(function($className) {
    $file = __DIR__ . '\\src\\' . $className . '.php';
	$file = str_replace(['\\', ROOT_NS], [DIRECTORY_SEPARATOR, ''], $file);
	if (file_exists($file)) {
		include_once $file;
	}
});
