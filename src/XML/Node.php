<?php
namespace LibreByte\XML;

use SimpleXMLElement;

class Node
{
    /**
     * @property SimpleXMLElement $reader
     */
    private $simpleXMLElement;

    public function __construct(SimpleXMLElement $simpleXMLElement)
    {
        $this->simpleXMLElement = $simpleXMLElement;
    }

    public function registerNamespace($prefix, $uri)
    {
        $this->simpleXMLElement->registerXPathNamespace($prefix, $uri);
    }

    public function getName()
    {
        return $this->simpleXMLElement->getName();
    }

    public function getValue()
    {
        return $this->__toString();
    }

    public function getAttribute($name, $defValue = null)
    {
        return $this->getChildValue("@{$name}", $defValue);
    }

    public function getChild(string $xpath, $defValue = null)
    {
        $node = $this->simpleXMLElement->xpath($xpath)[0] ?? $defValue;
        if ($node) {
            return new static($node);
        }
        return $defValue;
    }

    public function getChildValue(string $xpath, $defValue = null)
    {
        return (string) $this->getChild($xpath, $defValue);
    }

    public function getChildren(string $xpath = '', $defValue = [], $isPrefix = false)
    {
        $nodes = empty($xpath) || $isPrefix ? $this->simpleXMLElement->children($xpath, $isPrefix) : ($this->simpleXMLElement->xpath($xpath) ?? $defValue);
        foreach ($nodes as $node) {
            yield new static($node);
        }
    }

    public function asXML()
    {
        return $this->simpleXMLElement->asXML();
    }

    public function getNamespaces()
    {
        return $this->simpleXMLElement->getNamespaces();
    }

    public function getDocNamespaces()
    {
        return $this->simpleXMLElement->getDocNamespaces();
    }

    public function __toString()
    {
        return $this->simpleXMLElement->__toString();
    }
}
