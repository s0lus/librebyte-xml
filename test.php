<?php
ini_set('zend.assertions', 1);
ini_set('assert.exception', 1);

define('CLI', PHP_SAPI == 'cli');
define('NL', CLI ? PHP_EOL : '<br>');


require_once 'autoload.php';

use LibreByte\Tests\SimpleTest;

function success($funcName)
{
    if (CLI) {
        printf ("\033[32m ✓ $funcName".NL);
    } else {
        echo '<span style="color: green">', "✓ $funcName", '</span>', NL;
    }
    flush();
}

// https://stackoverflow.com/a/27147177
function progress_bar($done, $total, $desc = '')
{
    $perc = floor(($done / $total) * 100);
    $left = 100 - $perc;
    $write = sprintf("\033[0G\033[2K[%'={$perc}s>%-{$left}s] - $perc%% - $done/$total $desc", "", "");
    fwrite(STDERR, $write);
}

try {
    SimpleTest::autorun();
} catch (AssertionError $e) {
    if (CLI) {
        printf ("\033[31m 🗴 Failed {$e->getMessage()} on {$e->getFile()} (line: {$e->getLine()})".NL);
    } else {
        echo '<span style="color: red">', "🗴 Failed {$e->getMessage()} on {$e->getFile()} (line: {$e->getLine()})", '</span>', NL;
    }
}
